import { XMLBuilder } from 'fast-xml-parser';
import { Handler } from './handler';

export class XMLHandler extends Handler {
    constructor(ctx: any) {
        super(ctx);
    }

    override async handle() {   
        this.ctx.set.headers["Content-Type"] = "application/xml"
        return new XMLBuilder({ arrayNodeName: "root" }).build(await this.response())
    }
}