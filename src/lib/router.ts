import Elysia from "elysia";

export class Router {
    app: Elysia;

    // Constructor
    constructor(app: Elysia) {
        this.app = app;
    }

    // Define a GET route
    get(path: string, handler: any) {
        this.app.get(path, ctx => new handler(ctx).handle().then((x:any) => x))
        return this;
    }

}