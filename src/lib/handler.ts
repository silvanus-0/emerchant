
export class Handler {
    ctx: any;

    constructor(ctx: any) {
        this.ctx = ctx;
    }

    async response(): Promise<any> {
        return ""
    }

    async handle(): Promise<any> {
        return await this.response()
    }
}