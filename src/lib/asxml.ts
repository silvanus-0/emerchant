import { XMLBuilder } from "fast-xml-parser";


export const XML = (ctx: any, data: any) => {
    ctx.set.headers["Content-Type"] = "application/xml"
    return new XMLBuilder({ arrayNodeName: "root" }).build(data)
}
