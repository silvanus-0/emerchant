import { XMLHandler } from "../lib/xml.handler";

export class CategoriesHandler extends XMLHandler {
  async response(): Promise<any> {
    return {
      root: { categories: [{ name: "test", id: 1 }, { name: "test2", id: 2 }] },
    };
  }
}
