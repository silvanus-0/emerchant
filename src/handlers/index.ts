import { XMLHandler } from "../lib/xml.handler";

export class IndexHandler extends XMLHandler {
  constructor(ctx: any) {
    super(ctx);
  }

  override async response() {
    return {
      root: {
        app: "EMerchant",
        version: "0.3.0",
        engine: "Bun",
        timestamp: Date.now(),
      },
    };
  }
}
