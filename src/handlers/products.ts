import { XMLHandler } from "../lib/xml.handler";

export class ProductsHandler extends XMLHandler {
  async response(): Promise<any> {
    return {
      root: { products: [{ name: "test", id: 1 }, { name: "test2", id: 2 }] },
    };
  }
}
