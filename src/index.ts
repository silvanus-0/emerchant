import { Elysia } from "elysia";
import { Router } from "./lib/router";

import { IndexHandler } from "./handlers/index";
import { CategoriesHandler } from "./handlers/categories";
import { ProductsHandler } from "./handlers/products";



const app = new Elysia()
const router = new Router(app);

// Routes
router.get("/", IndexHandler);
router.get("/categories", CategoriesHandler);
router.get("/products", ProductsHandler);

// Run
app.listen(3000);

console.log(
  `🦊 Elysia is running at ${app.server?.hostname}:${app.server?.port}`,
);
