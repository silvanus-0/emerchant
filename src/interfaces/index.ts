
export interface DefaultResponse {
    app: string
    version: string
    engine: string
    timestamp: number
}

